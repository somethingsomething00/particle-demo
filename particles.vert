#version 430 core

out vec4 vf_color;

// Simulate glOrtho for 2d projection
uniform vec2 uOrtho;
uniform vec2 uOffset;
uniform float uScale;
const vec2 flip = vec2(1, -1);
const vec2 one = vec2(1);

struct ssbo_data
{
	// Storing these as vec2 won't work for size reasons
	// If this ever becomes a performance issue we'll have to see how to
	// guarantee the size of vec2 to be exactly 4 bytes
	float pos[2];
	float size[2];
	uint color;
};

layout(std430, binding = 2) buffer ssbo // Note: This name can be anything
{
	ssbo_data SsboData[];
};

// Since we're drawing quads, this allows us to apply the correct size offsets
// without branching
const vec2 VertexIDToSizeOffsetLut[4] = 
{
	vec2(-1, -1),
	vec2(-1, 1),
	vec2(1, 1),
	vec2(1, -1),
};

vec4 PixelUnpackLE(uint p)
{
	vec4 result;
	result.b = float((p >> (0 * 8)) & 0xFF) / 255.0f;
	result.g = float((p >> (1 * 8)) & 0xFF) / 255.0f;
	result.r = float((p >> (2 * 8)) & 0xFF) / 255.0f;
	result.a = float((p >> (3 * 8)) & 0xFF) / 255.0f;

	return result;
}

void main()
{
	uint DataIndex = gl_VertexID / 4;
	uint VertexIndex = gl_VertexID % 4;
	
	ssbo_data Data = SsboData[DataIndex];
	vec2 size = vec2(Data.size[0], Data.size[1]);
	vec2 halfsize = size * 0.5; // Should we perform this calculation on the cpu side?
	vec2 pos = vec2(Data.pos[0], Data.pos[1]);
	vec2 p = pos + (halfsize * VertexIDToSizeOffsetLut[VertexIndex]);

	p += uOffset; // Note: Pixel space

	vec2 pFinal = (p * uOrtho) - one;
	pFinal *= uScale;

	pFinal.y = -pFinal.y; // make y=0 be the top left

	gl_Position = vec4(pFinal.x, pFinal.y, 0, 1);
	vf_color = PixelUnpackLE(Data.color);
}
