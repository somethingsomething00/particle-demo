package particles

import "core:fmt"
import "core:os"
import "core:mem"

import gl "thirdparty/OpenGL"

ShaderCompile :: proc(shader: u32, source: string) -> bool
{
	data := os.read_entire_file(source) or_return
	cdata := make([]u8, len(data) + 1)
	defer delete(data)
	defer delete(cdata)

	copy(cdata, data)
	if len(cdata) > 0 do cdata[len(cdata) - 1] = 0
	ptr := (transmute(mem.Raw_Slice)cdata).data

	gl.ShaderSource(shader, 1, transmute([^]cstring)&ptr, nil)
	gl.CompileShader(shader)

	status: i32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status != 1
	{
		logLength: i32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)
		log := make([]u8, logLength)
		gl.GetShaderInfoLog(shader, logLength, nil, &log[0])
		if logLength > 0 do logLength -= 1 // Get rid of null terminator
		fmt.eprintf("ERROR in {}\n", source)
		fmt.eprintf("{}\n", string(log[:logLength]))

		return false
	}
	return true
}

ProgLink :: proc(prog: u32, vs: string, fs: string) -> bool
{
	status: i32
	gl.LinkProgram(prog)
	gl.GetProgramiv(prog, gl.LINK_STATUS, &status)
	if status != 1
	{
		logLength: i32
		gl.GetProgramiv(prog, gl.INFO_LOG_LENGTH, &logLength)
		log := make([]u8, logLength)
		gl.GetProgramInfoLog(prog, logLength, nil, &log[0])
		if logLength > 0 do logLength -= 1 // Get rid of null terminator
		fmt.eprintf("ERROR in {}, {}\n", vs, fs)
		fmt.eprintf("{}\n", string(log[:logLength]))

		return false
	}
	return true
}

ShaderLoad :: proc(vs: string, fs: string) -> (prog: u32, ok: bool)
{
	vsObj := gl.CreateShader(gl.VERTEX_SHADER)
	defer gl.DeleteShader(vsObj)

	fsObj := gl.CreateShader(gl.FRAGMENT_SHADER)
	defer gl.DeleteShader(fsObj)

	ShaderCompile(vsObj, vs) or_return
	ShaderCompile(fsObj, fs) or_return

	prog = gl.CreateProgram()
	gl.AttachShader(prog, vsObj)
	gl.AttachShader(prog, fsObj)
	ProgLink(prog, vs, fs) or_return

	return prog, true
}
