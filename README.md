# Particle Demo
A quad-based particle renderer that uses OpenGL's 4.x Shader Storage Buffer Objects for efficient vertex generation.

![thumbnail](./thumbnail.png)

```
Build:
./build.sh

Run:
./particle-demo [PARTICLE COUNT]
* Accepts an optional first argument to specify the particle count.


How it works:
- Data is passed in a buffer as x, y, w, h coordinates and a colour, 
- and the vertex shader generates the appropriate verticies based on this information.
- GLSL has a built-in gl_VertexID variable that gives the index of each vertex being processed for a given draw call.
- Since we need 4 verticies per quad, dividing this VertexID by 4 gives us the index in our data buffer.

- VertexID % 4 gives us the current vertex in a single quad. 
- A lookup table is used to apply the correct width and height offsets to the xy position.

Overall, this approach greatly minimises memory bandwidth. 
The older approach used a vertex buffer, which effectively means 4x more data (slow!).
To further save memory, we also pack the colour into a u32, and unpack it in the shader.

See RendererInit for more information on how the OpenGL state is set up.


Future possibilities:
- Use a dummy VBO and glMapBuffer to access the particle data directly.
- I've experimented with this, but it doesn't seem to give any performance benefits.
- Maybe I'm doing something incorrectly there.

- Move calculations to a compute shader? Never written one before but there could be something to it.


Controls:
WASD....Move camera
+.......Zoom in
-.......Zoom out
T.......Toggle thread mode
R.......Restart particles 
Z.......Print profile information
X.......Clear profile information
Q/ESC...Quit


Dependencies: 
OpenGL 4.3 (for shader support)
GLFW 3.3
Odin compiler (last tested with dev-2022-08:afec321d)

Troubleshooting:
If for some reason there's a black screen, try changing the variable ThreadsAreEnabled to false on startup.
Older versions of the Odin compiler seem to have a bug in the threading/sync code.

References:
- Odin Compiler
https://odin-lang.org/docs/install/

- OpenGL Docs
https://docs.gl/

- GLFW Docs
https://www.glfw.org/docs/latest/

- Xorshift
https://en.wikipedia.org/wiki/Xorshift
```
