package particles

// A basic xorshift psuedo-random number library
// Note: Must be seeded with a positive non-zero value

// Reference: https://en.wikipedia.org/wiki/Xorshift

rand_state_32 :: struct
{
	state: u32,
}

U32_MAX :: ~(cast(u32)0)

@(private)
_GlobalRandomState32: rand_state_32 = {state = 1234}

RandU32 :: proc() -> u32
{
	x: u32 = _GlobalRandomState32.state

	x ~= x << 13
	x ~= x >> 17
	x ~= x << 5

	_GlobalRandomState32.state = x
	return x
}

RandRangeExU32 :: proc(min: u32, max: u32) -> u32
{
	assert(max >= min)
	range := max - min
	r := RandU32() % range
	return r + min
}

RandF32 :: proc() -> f32
{
	return cast(f32)RandU32() / cast(f32)U32_MAX
}

RandRangeExF32 :: proc(min: f32, max: f32) -> f32
{
	assert(max >= min)
	range := max - min
	return RandF32() * range + min
}

RandZeroToOneF32 :: proc() -> f32
{
	return RandF32()
}
