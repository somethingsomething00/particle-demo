package particles
import "core:math"

len_v2 :: proc(v: v2f) -> f32
{
	imm1 := v * v
	// return math.sqrt(fabs(v.x) + fabs(v.y)) // Wrong, but looks cool!
	return math.sqrt(imm1.x + imm1.y)
}

// Is this faster or slower than odin's built-in abs?
fabs :: proc(n: f32) -> f32
{
	return transmute(f32) (transmute(u32)n & 0x7F_FF_FF_FF)
}
