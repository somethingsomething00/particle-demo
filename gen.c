#include <stdio.h>
#include <math.h>

float ToRadians(float n)
{
	return n * (M_PI / 180.0);
}

int main()
{
	typedef union
	{
		float f;
		unsigned int u;
	} lit;

	lit Sin;
	lit Cos;

	for(int i = 0; i < 360; i++)
	{
		float rad = ToRadians(i);
		Sin.f = sin(rad);
		Cos.f = cos(rad);

		printf("\tsincos[%d].s.u = %u\n", i, Sin.u);
		printf("\tsincos[%d].c.u = %u\n", i, Cos.u);
	}


	
	return 0;
}
