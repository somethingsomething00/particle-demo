package particles

import "core:time"
import "core:fmt"

timed_section_record :: struct
{
	elapsedSeconds: f64,
	hits: u64,
}

@(private)
TimedSections: map[string]timed_section_record


TimedSectionsReport :: proc()
{
	fmt.printf("========================================\n")
	fmt.printf("Reporting timed sections\n")
	for name, val in TimedSections
	{
		ms := (val.elapsedSeconds / cast(f64)val.hits) * 1000
		msTotal: f64 = val.elapsedSeconds * 1000
		fmt.printf("%-20s: {} ms ({} ms total)\n", name, ms, msTotal)
	}
	fmt.printf("========================================\n")
}

TimedSectionsClear :: proc()
{
	for _, val in &TimedSections
	{
		val.hits = 0
		val.elapsedSeconds = 0
	}
}


@(deferred_out=TimeSectionEnd)
TimedSection :: proc(SectionName: string) -> (time.Tick, ^timed_section_record)
{
	rec: ^timed_section_record
	timestamp: time.Tick
	exists: bool

	rec, exists  = &TimedSections[SectionName]



	if !exists
	{
		ts: timed_section_record
		TimedSections[SectionName] = ts
		rec = &TimedSections[SectionName]
	}
	else
	{
		// rec = &TimedSections[SectionName]
	}

	timestamp = time.tick_now()

	return timestamp, rec
}

@(private)
TimeSectionEnd :: proc(timestamp: time.Tick, record: ^timed_section_record)
{
	now := time.tick_now()
	elaspedDuration := time.tick_diff(timestamp, now)

	seconds := cast(f64)elaspedDuration / cast(f64)time.Second

	record.elapsedSeconds += seconds
	record.hits += 1
}

FpsReport :: proc(dt: f64)
{
	@static frameCount: u32
	@static accumulator: f64

	accumulator += dt
	frameCount += 1
	if frameCount == 240
	{
		fps := cast(f64)frameCount / accumulator
		fmt.printf("FPS: {}\n", fps)
		accumulator = 0
		frameCount = 0
	}
}
