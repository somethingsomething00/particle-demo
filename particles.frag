#version 430 core

in vec4 vf_color;
out vec4 FragColor;

void main()
{
	FragColor = vf_color;
}
