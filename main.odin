package particles

import "core:fmt"
import "core:runtime"
import "core:os"
import "core:strings"
import "core:strconv"
import "core:thread"
import "core:sync"

import gl "thirdparty/OpenGL"
import glfw "thirdparty/glfw"


/**********************************
* Constants
**********************************/
WIDTH :: 800
HEIGHT :: 600
MID_W :: WIDTH / 2
MID_H :: HEIGHT / 2
DEFAULT_PARTICLE_COUNT :: (uint)(20000)
PARTICLE_WIDTH :: 2
PARTICLE_HEIGHT :: 2
THREAD_COUNT :: 4
TARGET_DT_SECONDS :: 0.016666

/**********************************
* Types
**********************************/
v2f :: [2]f32

// Note: The layout of this struct MUST mirror ssbo_data in particles.vert
particle :: struct
{
	pos: v2f,
	w: f32,
	h: f32,
	colorPacked: u32,
}

// Particle properties that are needed, but do not conform to the data layout in ssbo_data
// They are stored as a separate array
particle_properties :: struct
{
	delta: v2f,
	distanceTravelled: f32,
}

renderer :: struct
{
	vao: u32,
	vbo: u32,
	ibo: u32,
	ssbo: u32,
	shader: u32,
	uOrtho: i32,
	uOffset: i32,
	uScale: i32,
}

surface :: struct
{
	offset: v2f,
	scale: f32,
}

client_window :: struct
{
	wWindowed: i32,
	hWindowed: i32,
	x: i32,
	y: i32,
}

thread_data :: struct
{
	part: []particle,
	prop: []particle_properties,
	id: int,
}

/**********************************
* Globals
**********************************/
Running: bool = true
Particles: []particle
ParticleProperties: []particle_properties
ClientWindow: client_window
ParticlesAreInitialised: bool = false
FrameDtInSeconds: f32
Surface: surface = {scale = 1.0}

BarrierMain: sync.Barrier
BarrierThread: sync.Barrier
Threads: [THREAD_COUNT]^thread.Thread
ThreadsAreEnabled: bool = true
ThreadStatusHasChanged: bool = false

argv := runtime.args__ // Is there a better way of getting this information? This is a bit cryptic...


SliceSizeInBytes :: proc(slice: []$T) -> int
{
	return len(slice) * size_of(slice[0])
}


RendererInit :: proc(vs: string, fs: string) -> renderer
{
	assert(ParticlesAreInitialised)

	// Note: Since we are using an SSBO, it looks like we don't need a VBO to render

	using R: renderer

	LOC_SSBO :: 2

	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	// Uncomment this code if rendering without a vbo does not work
	// gl.GenBuffers(1, &vbo)
	// gl.BindBuffer(gl.ARRAY_BUFFER, vbo)

	gl.GenBuffers(1, &ibo)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo)

	// Indexed rendering
	// Looks like we only need to submit the data once, and then it's safe to free


	indexBuffer := make([]u32, ParticleCount() * 6)
	defer delete(indexBuffer)
	offset: u32
	for i := 0; i < len(indexBuffer); i += 6
	{
		indexBuffer[i + 0] = offset + 0
		indexBuffer[i + 1] = offset + 1
		indexBuffer[i + 2] = offset + 2
		indexBuffer[i + 3] = offset + 2
		indexBuffer[i + 4] = offset + 3
		indexBuffer[i + 5] = offset + 0

		offset += 4
	}
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, SliceSizeInBytes(indexBuffer), raw_data(indexBuffer), gl.STREAM_DRAW)

	ok: bool
	shader, ok = ShaderLoad(vs, fs)
	if !ok
	{
		Exit()
	}
	gl.UseProgram(shader)

	uOrtho = gl.GetUniformLocation(shader, "uOrtho")
	uOffset = gl.GetUniformLocation(shader, "uOffset")
	uScale = gl.GetUniformLocation(shader, "uScale")

	gl.Uniform1f(uScale, Surface.scale)


	gl.GenBuffers(1, &ssbo)
	gl.BindBuffer(gl.SHADER_STORAGE_BUFFER, ssbo)
	gl.BufferData(gl.SHADER_STORAGE_BUFFER, SliceSizeInBytes(Particles), nil,  gl.STREAM_DRAW)
	gl.BindBufferBase(gl.SHADER_STORAGE_BUFFER, LOC_SSBO, ssbo)

	return R
}

RendererOrtho2D :: proc(R: ^renderer, width, height: f32)
{
	gl.Uniform2f(R.uOrtho, 2.0 / width, 2.0 / height)
}

RendererApplySurfaceOffset :: proc(R: ^renderer)
{
	using R
	gl.Uniform2f(uOffset,  Surface.offset.x, Surface.offset.y)

	scale := 1 / Surface.scale
	gl.Uniform1f(uScale, scale)
}

ParticleCount :: proc() -> int
{
	return len(Particles)
}

ParticleInit :: proc() -> (particle, particle_properties)
{
	using P: particle = ---
	using Prop: particle_properties = ---

	pos.x = MID_W
	pos.y = MID_H

	angle := RandRangeExU32(0, 360)
	s, c := SincosGet(angle)

	// Uncomment the following lines to keep the speed consistent to the frame rate
	// (Must remove speed constant)
	// targetSpeed :: 2.0 / TARGET_DT_SECONDS
	// speed := targetSpeed * FrameDtInSeconds


	speed :: 2
	// speed := RandRangeExF32(1.9, 2.1)
	delta.x = c * speed
	delta.y = s * speed

	w = PARTICLE_WIDTH
	h = PARTICLE_HEIGHT

	// b := cast(u8)RandRangeExU32(0, 1)
	// g := cast(u8)RandRangeExU32(8, 64)
	// r := cast(u8)RandRangeExU32(128, 256)
	b := cast(u8)RandRangeExU32(0, 256)
	g := cast(u8)RandRangeExU32(0, 256)
	r := cast(u8)0


	colorPacked = PixelPackRGBA_LE(r, g, b, 128)

	distanceTravelled = 0

	return P, Prop
}

// Just for fun - a memory pool allocator
when false
{
import "core:intrinsics"
Kilobyte :: 1024
Megabyte :: Kilobyte * 1024

Pool := make([]u8, Megabyte * 256)
PoolUsed: uint = 0
PoolAlloc :: proc($T: typeid, count: uint) -> T where intrinsics.type_is_slice(T)
{
	using runtime
	info := type_info_of(T).variant.(Type_Info_Slice)
	toAlloc := cast(uint)info.elem_size * count
	assert(PoolUsed + toAlloc < len(Pool))
	data: rawptr = &Pool[PoolUsed]

	PoolUsed += toAlloc

	result: Raw_Slice = ---

	result.data = data
	result.len = cast(int)count

	return transmute(T)result
}
}


ParticlesInit :: proc(count: uint)
{
	Particles = make([]particle, count)
	ParticleProperties = make([]particle_properties, count)

	// Particles = PoolAlloc([]particle, count)
	// ParticleProperties = PoolAlloc([]particle_properties, count)

	for p, i in &Particles
	{
		Prop := &ParticleProperties[i]
		p, Prop^  = ParticleInit()
	}

	ParticlesAreInitialised = true
}

ParticlesReset :: proc()
{
	assert(len(Particles)  == len(ParticleProperties))
	for p, i in &Particles
	{
		Prop := &ParticleProperties[i]

		p, Prop^ = ParticleInit()
	}
}


ThreadPoolInitAndLaunch :: proc()
{
	// Hack
	@static td: [THREAD_COUNT]thread_data
	assert(len(Threads) == len(td))
	assert(ParticlesAreInitialised)

	interval := ParticleCount() / len(Threads)

	range :: struct
	{
		begin: int,
		end: int,
	}

	ranges: [THREAD_COUNT]range

	sync.barrier_init(&BarrierMain, len(Threads) + 1)
	sync.barrier_init(&BarrierThread, len(Threads) + 1)

	rangeSum := 0
	for r in &ranges
	{
		using r
		begin = rangeSum
		end = rangeSum + interval
		rangeSum += interval
	}

	// This ensures that we include any remaining particles that are not divisible by THREAD_COUNT
	ranges[len(ranges) - 1].end = ParticleCount()

	for t, i in &Threads
	{
		begin := ranges[i].begin
		end := ranges[i].end
		td[i].part = Particles[begin:end]
		td[i].prop = ParticleProperties[begin:end]
		td[i].id = i
		t = thread.create_and_start_with_data(&td[i], ThreadProc)
		assert(t != nil)
	}
}


// Common version
_ParticlesUpdate :: proc(parts: []particle, props: []particle_properties)
{
	for i := 0; i < len(parts); i += 1
	{
		part := &parts[i]
		prop := &props[i]

		// Using len_v2 is correct, but simply adding the components looks nicer
		// prop.distanceTravelled += len_v2(prop.delta)
		prop.distanceTravelled += (fabs(prop.delta.x) + fabs(prop.delta.y))

		// p.w += 0.01
		// p.h += 0.01

		if prop.distanceTravelled > 400 // was 400
		{
			prop.delta *= 1.1
			part.w += 0.1
			part.h += 0.1
		}

		part.pos += prop.delta

		if (part.pos.x >= WIDTH || part.pos.y >= HEIGHT) || (part.pos.x < 0 || part.pos.y < 0)
		{
			part^, prop^ = ParticleInit()
		}
	}
}

// Single threaded version
ParticlesUpdate :: proc()
{
	TimedSection("Update Particles")
	_ParticlesUpdate(Particles[:], ParticleProperties[:])
}

// Threaded version
ThreadParticlesUpdate :: proc()
{
	// ThreadProc calls the update function
	// We synchronise the thread with the main thread by using these barriers
	sync.barrier_wait(&BarrierMain)
	TimedSection("Threaded Update Particles")
	sync.barrier_wait(&BarrierThread)
}

ThreadProc :: proc(d: rawptr)
{
	data := cast(^thread_data)d

	for
	{
		sync.barrier_wait(&BarrierMain)
		_ParticlesUpdate(data.part, data.prop)
		sync.barrier_wait(&BarrierThread)
	}
}

ParticlesSubmitToRenderer :: proc()
{
	TimedSection("Upload SSBO Data")
	gl.BufferData(gl.SHADER_STORAGE_BUFFER, SliceSizeInBytes(Particles), raw_data(Particles), gl.STREAM_DRAW)
}

CbKeys :: proc "c" (Window: glfw.WindowHandle, key, scancode, action, mod: i32)
{
	context = runtime.default_context()

	using glfw

	// Quit
	if (key == KEY_Q || key == KEY_ESCAPE) && action == PRESS
	{
		Running = false
	}

	// Toggle Fullscreen
	if key == KEY_F && action == PRESS
	{
		Monitor := glfw.GetWindowMonitor(Window)
		if Monitor != nil
		{
			glfw.SetWindowMonitor(Window, nil, ClientWindow.x, ClientWindow.y, ClientWindow.wWindowed, ClientWindow.hWindowed, glfw.DONT_CARE)
		}
		else
		{
			ClientWindow.x, ClientWindow.y = glfw.GetWindowPos(Window)
			ClientWindow.wWindowed, ClientWindow.hWindowed = glfw.GetWindowSize(Window)
			Monitor := glfw.GetPrimaryMonitor()
			Vidmode := glfw.GetVideoMode(Monitor)
			glfw.SetWindowMonitor(Window, Monitor, 0, 0, Vidmode.width, Vidmode.height, Vidmode.refresh_rate)
		}
	}

	// Reset surface properties
	if key == KEY_0 && action == PRESS
	{
		Surface.offset = 0
		Surface.scale = 1
	}

	// Toggle threads
	if key == KEY_T && action == PRESS
	{
		ThreadStatusHasChanged = true
	}

	if key == KEY_R && action == PRESS
	{
		ParticlesReset()
	}

	// Profiler
	if key == KEY_Z && action == PRESS
	{
		TimedSectionsReport()
	}

	if key == KEY_X && action == PRESS
	{
		TimedSectionsClear()
	}
}

CbResize :: proc "c" (Window: glfw.WindowHandle, x, y: i32)
{
	gl.Viewport(0, 0, x, y)
}

CursorHide :: proc(Window: glfw.WindowHandle)
{
	glfw.SetInputMode(Window, glfw.CURSOR, glfw.CURSOR_HIDDEN)
}

PollKeys :: proc(Window: glfw.WindowHandle, dt: f32)
{
	using glfw

	KeyPressed :: proc(Window: glfw.WindowHandle, key: i32) -> bool
	{
		return GetKey(Window, key) == PRESS
	}


	panspeed :: 400
	scalespeed := dt

	offspeed := panspeed * Surface.scale * dt

	// Right
	if KeyPressed(Window, KEY_D)
	{
		Surface.offset.x -= offspeed
	}

	// Left
	if KeyPressed(Window, KEY_A)
	{
		Surface.offset.x += offspeed
	}

	// Up
	if KeyPressed(Window, KEY_W)
	{
		Surface.offset.y += offspeed
	}

	// Down
	if KeyPressed(Window, KEY_S)
	{
		Surface.offset.y -= offspeed
	}

	// Zoom in
	if KeyPressed(Window, KEY_EQUAL)
	{
		scale := max(0.05, Surface.scale - scalespeed)
		Surface.scale = scale
	}

	// Zoom out
	if KeyPressed(Window, KEY_MINUS)
	{
		Surface.scale += scalespeed
	}
}

PixelPackRGBA_LE :: proc(r, g, b, a: u8) -> u32
{
	result :=
	cast(u32) b << (0 * 8) |
	cast(u32) g << (1 * 8) |
	cast(u32) r << (2 * 8) |
	cast(u32) a << (3 * 8)

	return result
}


// Odin will complain if you directly call os.exit() before the end of a procedure body
// This is way to get around odin's automatic detection of os.exit()
Exit :: proc(code: int = 0)
{
	os.exit(code)
}

main :: proc()
{
	NewParticleCount: uint = DEFAULT_PARTICLE_COUNT

	if len(argv) > 1
	{
		arg := argv[1]

		// Try to support utf8?
		// Not sure if this helps
		s := strings.clone_from_cstring_bounded(arg, 64)
		defer delete(s)

		count, ok := strconv.parse_u64(s)

		if ok
		{
			NewParticleCount = cast(uint)count
		}
		else
		{
			prog := argv[0]
			fmt.printf("ERROR: Expected a positive integer, but got '{}'\n", s)
			fmt.printf("Defaulting to {}\n", NewParticleCount)
			fmt.printf("Usage: {} [PARTICLE COUNT]\n", prog)
		}
	}

	SWAP_INTERVAL :: 1

	glfw.Init()
	Monitor := glfw.GetPrimaryMonitor()
	Vidmode := glfw.GetVideoMode(Monitor)

	glfw.WindowHint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
	glfw.WindowHint(glfw.CONTEXT_VERSION_MAJOR, 3)
	glfw.WindowHint(glfw.CONTEXT_VERSION_MINOR, 3)

	Window := glfw.CreateWindow(Vidmode.width, Vidmode.height, "Particle Demo", Monitor, nil)

	ClientWindow.wWindowed = Vidmode.width / 2
	ClientWindow.hWindowed = Vidmode.height / 2
	ClientWindow.x = Vidmode.width / 2 - ClientWindow.wWindowed / 2
	ClientWindow.y = Vidmode.height / 2 - ClientWindow.hWindowed / 2

	FrameDtInSeconds = 1.0 / (cast(f32)Vidmode.refresh_rate / SWAP_INTERVAL)

	glfw.MakeContextCurrent(Window)
	glfw.SwapInterval(SWAP_INTERVAL)


	glfw.SetKeyCallback(Window, CbKeys)
	glfw.SetFramebufferSizeCallback(Window, CbResize)

	CursorHide(Window)

	gl.load_up_to(3, 3, glfw.gl_set_proc_address)

	gl.Enable(gl.BLEND)
	gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	gl.ClearColor(0, 0, 0, 1)

	SincosInit()
	ParticlesInit(NewParticleCount)

	Renderer := RendererInit("particles.vert", "particles.frag")
	RendererOrtho2D(&Renderer, WIDTH, HEIGHT)

	fmt.printf("Rendering {} particles\n", ParticleCount())

	ThreadPoolInitAndLaunch()

	/////////////
	// Main loop
	/////////////
	for Running
	{
		t1 := glfw.GetTime()
		TimedSection("Frame")
		glfw.PollEvents()
		PollKeys(Window, FrameDtInSeconds)
		RendererApplySurfaceOffset(&Renderer)
		gl.Clear(gl.COLOR_BUFFER_BIT)
		{
			TimedSection("Update and Submit")

			if ThreadsAreEnabled
			{
				ThreadParticlesUpdate()
			}
			else
			{
				ParticlesUpdate()
			}

			ParticlesSubmitToRenderer()
		}
		{
			TimedSection("Draw")
			gl.DrawElements(gl.TRIANGLES, cast(i32)ParticleCount() * 6, gl.UNSIGNED_INT, nil)
		}
		tBeforeSwap := glfw.GetTime()
		FpsReport(tBeforeSwap - t1)
		glfw.SwapBuffers(Window)
		t2 := glfw.GetTime()
		FrameDtInSeconds = cast(f32)(t2 - t1)

		if ThreadStatusHasChanged
		{
			ThreadStatusHasChanged = false
			ThreadsAreEnabled = !ThreadsAreEnabled

			threadState: string = ThreadsAreEnabled ? "ON" : "OFF"
			fmt.printf("Threads: {}\n", threadState)
		}
	}
	glfw.Terminate()
}
